from multiprocessing import Pool, cpu_count, Queue, freeze_support
from models import Block, get_algo
from copy import deepcopy
from random import randint
from datetime import datetime
import sys

b = Block.fromjson(
    {
        "content": '{"from": 123, "to": 12314, "amount": 123.1}',
        "meta_id": "4c4e027f-413f-4f96-b0c8-4aa5bb8933c5",
        "id": None,
        "parent": "0000093a948887d8398bf9266a66332a36650f95",
        "verify_token": None,
        "creation_date": "2021-12-16T13:36:58.095670+00:00",
    }
)
q = Queue()
stuff_verify = ("0" * 5, get_algo("sha1"))
count_proc = cpu_count()


def wrapper(n):
    block_size = sys.maxsize // count_proc
    bn = deepcopy(b)
    while True:
        i = randint(block_size * n, (block_size * (n + 1)) - 1)
        bn.verify_token = i
        if bn.verify(*stuff_verify):
            q.put(i)


if __name__ == "__main__":
    freeze_support()

    p = Pool(count_proc)
    start_at = datetime.now()
    print("Start at %s" % (start_at,))
    _ = p.map_async(wrapper, range(1, count_proc + 1))

    b.verify_token = q.get()
    print("Verify code is %d" % (b.verify_token,))
    print("Verification is %r" % (b.verify(*stuff_verify),))
    print("Hash of token %s" % (b.id,))

    p.close()
    finish_at = datetime.now()
    print("Finish at %s" % (finish_at,))
    diff = finish_at - start_at
    print("Duration %s" % (diff,))
