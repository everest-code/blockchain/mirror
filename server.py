from tornado.web import Application, GZipContentEncoding
from tornado.escape import json_decode, json_encode
from logging import getLogger, basicConfig
from multiprocessing.managers import ValueProxy
from config import config, get_blockhain_conf
from models import Blockchain, WaitList, Block
from tornado.httpclient import HTTPClient, HTTPRequest

from routes import (
    BlockchainConfigHandler,
    LastBlockHandler,
    BlockchainHandler,
    BlockchainContentHandler,
)
from routes.blocks import BlockStatusHandler
from routes.mirrors import SyncHandler, ConsensusHandler
from routes.waitlist import WaitListHandler, LastWaitBlockHandler, GetWaitBlockHandler
from routes.miners import MinersSyncHandlers, MinersBlockHandler

_log = getLogger("server")
basicConfig(
    format="[%(asctime)s | %(levelname)s][%(name)s %(process)d]: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S %Z",
)


def sync_on_mirror(actual_node: str, node: str, blockchain: ValueProxy[Blockchain]):
    _log.info("Start sync with %s", node)
    cli = HTTPClient()

    # Sync on a mirror
    req = HTTPRequest(
        f"{node}/sync",
        method="POST",
        headers={
            "Content-Type": "application/json",
        },
        body=json_encode(actual_node),
    )
    res = json_decode(cli.fetch(request=req, raise_error=False))
    if not res["ok"]:
        _log.warning("This node is already on mirrorlist")

    # Get last blockchain
    req = HTTPRequest(f"{node}/blockchain")
    res = json_decode(cli.fetch(request=req, raise_error=False))

    if res["ok"] and len(blockchain.value.chain) == 0:
        blockchain.value.chain.extend([Block.fromjson(data) for data in res["payload"]])

    cli.close()
    _log.debug("Finish sync with %s", node)


def make_app(debug: bool = False) -> Application:
    basicConfig(
        level=config()["log_level"] if not debug else "DEBUG",
        format="[%(asctime)s | %(levelname)s][%(name)s %(process)d]: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S %Z",
        force=True,
    )

    if debug:
        _log.warning(
            "Debug mode and autoreload are active, if is in a production mode desable it"
        )

    # List cache to get and handle other node mirrors and miners
    node_mirrors = list()
    node_miners = list()

    actual_node = config()["actual_node"]  # URL of server node
    mirror_config = config()["mirror"]
    blockchain_conf = get_blockhain_conf()
    blockchain = Blockchain(config=blockchain_conf)
    waitlist = WaitList()

    if mirror_config["mirror_node"] != "<null>":
        sync_on_mirror(actual_node, mirror_config["mirror_node"], blockchain)
    else:
        _log.debug("Omiting mirror sync")

    routes = [
        # fmt:off
        # Blockchain Basics
        (r"^/blockchain/config$", BlockchainConfigHandler, dict(config=blockchain_conf)),
        (r"^/blockchain/last$", LastBlockHandler, dict(blockchain=blockchain)),
        (r"^/blockchain$", BlockchainHandler, dict(blockchain=blockchain)),
        (r"^/blockchain/content$", BlockchainContentHandler, dict(blockchain=blockchain)),
        (r"^/blockchain/add$", MinersBlockHandler, dict(
            cache=node_mirrors,
            cache_miners=node_miners,
            blockchain=blockchain,
            waitlist=waitlist,
            consensus_on=mirror_config['sync_on'],
        )),

        # Block status
        (r"/block/status/(?P<id>[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})", BlockStatusHandler, dict(
            waitlist=waitlist,
            blockchain=blockchain,
        )),

        # Mirrorlist consensus
        (r"^/consensus$", ConsensusHandler, dict(
            actual_node=actual_node,
            cache=node_mirrors,
            cache_miners=node_miners,
            blockchain=blockchain,
        )),

        # Mirrors sync
        (r"^/sync$", SyncHandler, dict(
            actual_node=actual_node,
            cache=node_mirrors,
        )),

        # Miners sync
        (r"/sync/miners", MinersSyncHandlers, dict(cache=node_miners)),

        # Waitlist handling
        (r'/waitlist/(?P<id>[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})', GetWaitBlockHandler, dict(waitlist=waitlist)),
        (r"/waitlist", WaitListHandler, dict(waitlist=waitlist)),
        (r"/waitlist/last", LastWaitBlockHandler, dict(
            waitlist=waitlist,
            blockchain=blockchain,
        )),
        # fmt:on
    ]

    _log.debug("%d routes loaded", len(routes))

    return Application(
        routes,
        transforms=[GZipContentEncoding],
        autoreload=debug,
        debug=debug,
    )
