from tornado.web import RequestHandler
from models import BlockchainConfig, Blockchain


class BlockchainConfigHandler(RequestHandler):
    config: BlockchainConfig

    def initialize(self, config: BlockchainConfig):
        setattr(self, "config", config)

    def get(self):
        self.finish(
            {
                "ok": True,
                "payload": self.config.asjson(),
            }
        )


class LastBlockHandler(RequestHandler):
    blockchain: Blockchain

    def initialize(self, blockchain: Blockchain):
        setattr(self, "blockchain", blockchain)

    def get(self):
        block = self.blockchain.get_last()

        self.finish(
            {
                "ok": True,
                "payload": block.asjson(),
            }
        )


class BlockchainContentHandler(RequestHandler):
    blockchain: Blockchain

    def initialize(self, blockchain: Blockchain):
        setattr(self, "blockchain", blockchain)

    def get(self):
        raw_chain = self.blockchain.chain[:]
        if (tail := self.get_argument("tail", "-1")) != "-1":
            tail = int(tail)
            raw_chain = raw_chain[-tail:]

        chain = list(map(lambda el: el.ascontentjson(), raw_chain))

        self.finish(
            {
                "ok": True,
                "payload": chain,
            }
        )


class BlockchainHandler(RequestHandler):
    blockchain: Blockchain

    def initialize(self, blockchain: Blockchain):
        setattr(self, "blockchain", blockchain)

    def get(self):
        raw_chain = self.blockchain.chain[:]
        if (tail := self.get_argument("tail", "-1")) != "-1":
            tail = int(tail)
            raw_chain = raw_chain[-tail:]

        chain = list(map(lambda el: el.asjson(), raw_chain))

        self.finish(
            {
                "ok": True,
                "payload": chain,
            }
        )
