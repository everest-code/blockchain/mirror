from utils.handler import LogRequestHandler
from models import Blockchain, Block, WaitList
from uuid import UUID


class BlockStatusHandler(LogRequestHandler):
    blockchain: Blockchain
    waitlist: WaitList

    def initialize(self, blockchain: Blockchain, waitlist: WaitList):
        setattr(self, "blockchain", blockchain)
        setattr(self, "waitlist", waitlist)

    def get(self, id: str):
        status = "NOT_FOUND"
        if (wait_id := self.waitlist.as_hash_table.get(id, None)) is not None:
            block = self.waitlist[wait_id]
            if block.in_use:
                status = "MINING"
            else:
                status = "IDLE"
        elif self.blockchain.find_by_meta_id(UUID(id)) is not None:
            status = "BLOCKCHAIN"

        self.set_status(404 if status == "NOT_FOUND" else 200)
        self.finish(
            {
                "ok": True,
                "payload": status,
            }
        )
