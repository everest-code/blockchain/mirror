from utils.handler import LogRequestHandler
from tornado_middleware.commons import JSONSchemaValidationMiddleware
from tornado.ioloop import IOLoop
from typing import List
from routes.mirrors import UpdateConsensus
from tornado.escape import json_decode
from models import Blockchain, Block, WaitList


class MinersSyncHandlers(LogRequestHandler, JSONSchemaValidationMiddleware):
    schema = {
        "type": "string",
        "pattern": r"^https?:\/\/[\w\-\.]+(:\d{2,4})?$",
    }

    cache: List[str]

    def initialize(self, cache: List[str]):
        setattr(self, "cache", cache)

    def get(self):
        self.set_status(200)
        return self.finish({"ok": True, "payload": self.cache[:]})

    def post(self):
        data = json_decode(self.request.body)
        if data in self.cache:
            self.set_status(400)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": f"node '{data}' already in miners pool",
                }
            )

        self.cache.append(data)
        self.set_status(202)
        return self.finish(
            {
                "ok": True,
                "payload": None,
            }
        )


class MinersBlockHandler(
    UpdateConsensus, LogRequestHandler, JSONSchemaValidationMiddleware
):
    cache_miners: List[str]
    cache: List[str]
    blockchain: Blockchain
    waitlist: WaitList
    consensus_on: int

    schema = {
        "type": "object",
        "required": [
            "miner",
            "block",
        ],
        "additionalProperties": False,
        "properties": {
            "miner": {
                "type": "string",
                "pattern": r"^https?:\/\/[\w\-\.]+(:\d{2,4})?$",
            },
            "block": {
                "type": "object",
                "required": [
                    "meta_id",
                    "parent",
                    "content",
                    "verify_token",
                    "creation_date",
                ],
                "additionalProperties": False,
                "properties": {
                    "id": {"type": ["string", "null"]},
                    "meta_id": {
                        "type": "string",
                        "pattern": r"^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$",
                    },
                    "parent": {"type": ["string", "null"]},
                    "content": {
                        "type": "string",
                    },
                    "verify_token": {
                        "type": "number",
                        "exclusiveMinimun": 0,
                    },
                    "creation_date": {
                        "type": "string",
                    },
                },
            },
        },
    }

    def initialize(
        self,
        cache_miners: List[str],
        cache: List[str],
        blockchain: Blockchain,
        waitlist: WaitList,
        consensus_on: int,
    ):
        setattr(self, "cache_miners", cache_miners)
        setattr(self, "cache", cache)
        setattr(self, "blockchain", blockchain)
        setattr(self, "waitlist", waitlist)
        setattr(self, "consensus_on", consensus_on)

    def post(self):
        data = json_decode(self.request.body)
        block_raw, miner = data["block"], data["miner"]

        if miner not in self.cache_miners:
            self.set_status(401)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": "miner not in miners list pleas sign up first",
                }
            )

        block = Block.fromjson(block_raw)

        if (last_block := self.blockchain.get_last()) is not None:
            if block.parent != last_block.id:
                self.set_status(400)
                return self.finish(
                    {
                        "ok": False,
                        "payload": None,
                        "error": "block does not match with the last block",
                    }
                )

        if self.blockchain.add_block(block) == -1:
            self.set_status(500)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": "block sent is not valid",
                }
            )

        await_list = [self.update_miner(miner, block.id) for miner in self.cache_miners]
        for future in await_list:
            IOLoop().current().add_callback(self.summary, future)
        if (len(self.blockchain.chain) % self.consensus_on) == 0:
            await_list = [
                self.consensus_mirror(mirror, self.blockchain.chain)
                for mirror in self.cache
            ]
            for future in await_list:
                IOLoop().current().add_callback(self.summary, future)

        self.set_status(201)
        self.finish(
            {
                "ok": True,
                "payload": block.asjson(),
            }
        )
