from utils.handler import LogRequestHandler
from tornado.escape import json_decode, json_encode
from tornado_middleware.commons import JSONSchemaValidationMiddleware
from models import WaitList, WaitBlock, Blockchain
from uuid import UUID


class GetWaitBlockHandler(LogRequestHandler):
    waitlist: WaitList

    def initialize(self, waitlist: WaitList):
        setattr(self, "waitlist", waitlist)

    def get(self, id: str):
        try:
            id = UUID(id)
        except Exception:
            self.set_status(400)
            return self.finish(
                {"ok": False, "payload": None, "error": "uuid is not valid"}
            )
        else:
            idx = self.waitlist.as_hash_table[str(id)]
            block = self.waitlist[idx]
            self.finish({"ok": True, "payload": block.asjson()})

    def put(self, id: str):
        try:
            id = UUID(id)
        except Exception:
            self.set_status(400)
            return self.finish(
                {"ok": False, "payload": None, "error": "uuid is not valid"}
            )
        else:
            try:
                self.waitlist.toggle_in_use(id)
            except IndexError:
                self.set_status(404)
                self.finish(
                    {
                        "ok": False,
                        "payload": None,
                        "error": "block was not found",
                    }
                )
            else:
                self.set_status(202)
                self.finish(
                    {
                        "ok": True,
                        "payload": "changed",
                    }
                )

    def delete(self, id: str):
        try:
            id = UUID(id)
        except Exception:
            self.set_status(400)
            return self.finish(
                {"ok": False, "payload": None, "error": "uuid is not valid"}
            )
        else:
            try:
                self.waitlist.pop(id)
            except IndexError:
                self.set_status(404)
                self.finish(
                    {
                        "ok": False,
                        "payload": None,
                        "error": "block was not found",
                    }
                )
            else:
                self.set_status(202)
                self.finish(
                    {
                        "ok": True,
                        "payload": "deleted",
                    }
                )


class LastWaitBlockHandler(LogRequestHandler):
    waitlist: WaitList
    blockchain: Blockchain

    def initialize(self, waitlist: WaitList, blockchain: Blockchain):
        setattr(self, "waitlist", waitlist)
        setattr(self, "blockchain", blockchain)

    def get(self):
        if len(self.waitlist.not_in_use) == 0:
            self.set_status(404)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": "block was not found",
                }
            )

        last_block = self.blockchain.get_last()
        last_id = None if last_block is None else last_block.id
        last_waitblock = self.waitlist.not_in_use[-1]

        self.finish(
            {
                "ok": True,
                "payload": last_waitblock.as_block(last_id).asjson(),
            }
        )


class WaitListHandler(LogRequestHandler, JSONSchemaValidationMiddleware):
    waitlist: WaitList
    schema = {}

    def initialize(self, waitlist: WaitList):
        setattr(self, "waitlist", waitlist)

    def get(self):
        data = list(map(lambda el: el.asjson(), self.waitlist))
        if (tail := self.get_argument("tail", "-1")) != "-1":
            tail = int(tail)
            data = data[-tail:]
        elif self.get_argument("last", "off") == "on" and len(data) > 0:
            data = data[-1]

        if len(data) == 0:
            self.set_status(404)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": "The waitlist is empty",
                }
            )
        self.finish(
            {
                "ok": True,
                "payload": data,
            }
        )

    def post(self):
        data = json_decode(self.request.body)

        if data is None:
            self.set_status(400)
            return self.finish(
                {
                    "ok": True,
                    "payload": None,
                    "error": "payload is empty",
                }
            )

        block = WaitBlock.fromjson(json_encode(data))
        self.waitlist.append(block)

        self.set_status(201)
        self.finish(
            {
                "ok": True,
                "payload": block.asjson(),
            }
        )
