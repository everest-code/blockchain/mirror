from tornado_middleware.commons import JSONSchemaValidationMiddleware
from tornado.escape import json_decode, json_encode
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.ioloop import IOLoop
from utils.handler import LogRequestHandler
from models import Blockchain, Block
from typing import List, Optional, Awaitable
from functools import reduce
from uuid import UUID


class MirrorsHandler(LogRequestHandler):
    cache: List[str]
    actual_node: str

    def initialize(self, cache: List, actual_node: str):
        setattr(self, "cache", cache)
        setattr(self, "actual_node", actual_node)


class UpdateConsensus(LogRequestHandler):
    client: AsyncHTTPClient

    def prepare(self):
        self.client = AsyncHTTPClient()

    async def consensus_mirror(self, mirror: str, chain: List[Block]) -> Optional[str]:
        req = HTTPRequest(
            url=f"{mirror}/consensus",
            method="POST",
            headers={
                "Content-Type": "application/json",
            },
            body=json_encode(
                {
                    "source": self.actual_node,
                    "blockchain": [block.asjson() for block in chain],
                }
            ),
        )

        try:
            res = await self.client.fetch(request=req, raise_error=False)
        except Exception as err:
            err_str = f"{err.__class__.__name__}: {str(err)}"
            self.log.error(err_str)
            return err_str
        else:
            if res.code != 201:
                return None

            return json_decode(res.body)["error"]

    async def update_miner(self, miner: str, block_id: UUID) -> Optional[str]:
        req = HTTPRequest(
            url=f"{miner}/update",
            method="POST",
            headers={
                "Content-Type": "application/json",
            },
            body=json_encode(str(block_id)),
        )

        try:
            res = await self.client.fetch(request=req, raise_error=False)
        except Exception as err:
            err_str = f"{err.__class__.__name__}: {str(err)}"
            self.log.error(err_str)
            return err_str
        else:
            if res.code != 201:
                return None

            return json_decode(res.body)["error"]

    async def summary(self, err: Awaitable[Optional[str]]):
        err = await err
        if err is not None:
            self.log.warning("A mirror raise an error: %s", err)


class SyncHandler(MirrorsHandler, JSONSchemaValidationMiddleware):
    schema = {
        "type": "string",
        "pattern": r"^https?:\/\/[\w\-\.]+(:\d{2,4})?$",
    }

    def get(self):
        self.set_status(200)
        return self.finish(
            {
                "ok": True,
                "payload": self.cache[:] + [self.actual_node],
            }
        )

    def post(self):
        data = json_decode(self.request.body)
        if data in self.cache:
            self.set_status(400)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": f"node '{data}' already in mirrorlist",
                }
            )

        self.cache.append(data)
        self.set_status(202)
        return self.finish(
            {
                "ok": True,
                "payload": None,
            }
        )


class ConsensusHandler(UpdateConsensus, MirrorsHandler, JSONSchemaValidationMiddleware):
    cache_miners: List[str]
    blockchain: Blockchain
    schema = {
        "type": "object",
        "required": [
            "source",
            "blockchain",
        ],
        "additionalProperties": False,
        "properties": {
            "source": {
                "type": "string",
                "pattern": r"^https?:\/\/[\w\-\.]+(:\d{2,4})?$",
            },
            "blockchain": {
                "type": "array",
                "items": {
                    "type": "object",
                    "required": [
                        "id",
                        "parent",
                        "content",
                        "verify_token",
                        "creation_date",
                    ],
                    "additionalProperties": False,
                    "properties": {
                        "id": {
                            "type": "string",
                            "pattern": r"^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$",
                        },
                        "parent": {
                            "type": ["string", "null"],
                            "pattern": r"^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$",
                        },
                        "content": {
                            "type": "string",
                        },
                        "verify_token": {
                            "type": "number",
                            "exclusiveMinimun": 0,
                        },
                        "creation_date": {
                            "type": "string",
                        },
                    },
                },
            },
        },
    }

    def initialize(
        self,
        cache: List[str],
        cache_miners: List[str],
        actual_node: str,
        blockchain: Blockchain,
    ):
        setattr(self, "cache", cache)
        setattr(self, "cache_miners", cache_miners)
        setattr(self, "actual_node", actual_node)
        setattr(self, "blockchain", blockchain)

    def get(self):
        if (block := self.blockchain.get_last()) is None:
            self.set_status(500)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": "blockchain is empty yet",
                }
            )

        self.finish(
            {
                "ok": True,
                "payload": {
                    "creation_date": block.creation_date.isoformat(),
                    "id": str(block.id),
                    "parent": str(block.parent) if block.parent is not None else None,
                },
            }
        )

    def post(self):
        data = json_decode(self.request.body)
        blockchain_extern, source = data["blockchain"], data["source"]
        if source not in self.cache:
            self.set_status(403)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": f"source '{source}' not in mirrorlist, sync/register your node first",
                }
            )

        if len(self.blockchain.chain) > len(blockchain_extern):
            self.set_status(500)
            return self.finish(
                {
                    "ok": False,
                    "payload": None,
                    "error": f"the target blockchain is shorter than the source, download the blockchain in '{self.actual_node}/blockchain'",
                }
            )

        blockchain_extern: List[Block] = reduce(
            lambda acc, el: list(*acc, Block.fromjson(el)),
            blockchain_extern,
            list(),
        )

        for block in blockchain_extern:
            if block.verify(
                self.blockchain.config.prefix,
                self.blockchain.config.hash_algo,
            ):
                self.set_status(400)
                return self.finish(
                    {
                        "ok": False,
                        "payload": None,
                        "error": "blockchain integrity is compromised, download the blockchain in '{self.actual_node}/blockchain'",
                    }
                )

        self.blockchain.chain.clear()
        self.blockchain.chain.extend(blockchain_extern)
        consensus = [
            self.consensus_mirror(mirror, self.blockchain.chain)
            for mirror in filter(lambda url: url != source, self.cache)
        ]
        for future in consensus:
            IOLoop().current().add_callback(self.summary, future)

        miners_update = [
            self.update_miner(miner, self.blockchain.get_last().id)
            for miner in self.cache_miners
        ]
        for future in miners_update:
            IOLoop().current().add_callback(self.summary, future)

        self.set_status(201)
        self.finish(
            {
                "ok": True,
                "payload": "changed",
            }
        )
