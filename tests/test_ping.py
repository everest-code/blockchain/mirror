import unittest
from utils.ping import ping_port, DNSException


class PingPortTest(unittest.TestCase):
    data = ("google.com", 80)
    data_dns_error = ("asdf", 80)
    data_false = ("localhost", 1200)

    def test_ok_port(self):
        self.assertEqual(ping_port(*self.data), True)

    def test_error_dns(self):
        with self.assertRaises(DNSException):
            self.assertEqual(ping_port(*self.data_dns_error), False)

    def test_port_error(self):
        self.assertEqual(ping_port(*self.data_false), False)


if __name__ == "__main__":
    unittest.main()
