import unittest
from models import timedelta, Block, Blockchain, BlockchainConfig
from utils import get_algo
from copy import deepcopy


class BlockTest(unittest.TestCase):
    block = Block.fromjson(
        {
            "content": '{"ok": true}',
            "meta_id": "dc97bbdc-b9d5-3567-9159-317965eb5454",
            "parent": None,
            "creation_date": "2021-12-02T15:29:03.224310-05:00",
        }
    )

    block_non_json = Block.fromjson(
        {
            "content": "hola que hace",
            "meta_id": "dc97bbdc-b9d5-3567-9159-317965eb5454",
            "parent": None,
            "creation_date": "2021-12-02T15:29:03.224310-05:00",
        }
    )

    def test_json_convert(self):
        self.assertEqual(self.block.content_dict, {"ok": True})

        with self.assertRaises(RuntimeError):
            _ = self.block_non_json.content_dict

    def test_get_hash(self):
        self.assertEqual(
            self.block.get_hash(get_algo("sha1")),
            "59abfe3161562b5f60f810ae8e019e2ed8f28eaa",
        )

    def test_verification(self):
        block = self.block.copy()
        block.verify_token = 2808946793705991523
        self.assertTrue(block.verify("0" * 3, get_algo("sha1")))
        self.assertFalse(block.verify("0" * 3, get_algo("sha256")))


class BlockchainConfigTest(unittest.TestCase):
    conf = BlockchainConfig(3, "0", timedelta(minutes=3), get_algo("sha1"))

    def test_get_algorithm_name(self):
        self.assertEqual(self.conf.hash_algo_name, "sha1")

    def test_prefix(self):
        self.assertEqual(self.conf.prefix, "000")

    def test_difficulty_upper(self):
        conf = self.conf.copy()
        time = conf.tolerance + timedelta(seconds=120)

        conf.change_difficulty(time)
        self.assertEqual(conf.difficulty, 2)

    def test_difficulty_negative(self):
        conf = self.conf.copy()
        time = conf.tolerance + timedelta(seconds=120)

        conf.difficulty = 1
        conf.change_difficulty(time)
        self.assertEqual(conf.difficulty, 1)

    def test_difficulty_lower(self):
        conf = self.conf.copy()
        time = conf.tolerance - timedelta(seconds=120)

        conf.change_difficulty(time)
        self.assertEqual(conf.difficulty, 4)


class BlockchainTest(unittest.TestCase):
    chain = Blockchain(BlockchainConfigTest.conf)

    def test_add_block(self):
        block = BlockTest.block.copy()
        block.verify_token = 2808946793705991523

        chain = deepcopy(self.chain)
        self.assertEqual(chain.add_block(block), 0)

    def get_last(self):
        block = BlockTest.block.copy()
        block.verify_token = 2808946793705991523

        chain = deepcopy(self.chain)
        self.assertEqual(chain.get_last(), None)
        self.assertEqual(chain.add_block(block), 0)
        self.assertEqual(chain.get_last(), block)


# TODO: Create test of the resting models
if __name__ == "__main__":
    unittest.main()
