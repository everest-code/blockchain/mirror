import unittest
from datetime import datetime, timezone
from utils import tz_now, create_uuid, get_localzone, get_algo, parse_date


class TimezoneTest(unittest.TestCase):
    def test_tz(self):
        self.assertEqual(tz_now().tzinfo, get_localzone())

    def test_date_clock(self):
        now = datetime.now()
        now_tz = tz_now().replace(tzinfo=None)

        self.assertEqual(now_tz.date(), now.date())
        self.assertEqual(now_tz.hour, now.hour)
        self.assertEqual(now_tz.minute, now.minute)
        self.assertEqual(now_tz.second, now.second)

    def test_parse_date(self):
        date_str, date = "2021-12-07T02:05:58Z", datetime(
            2021, 12, 7, 2, 5, 58
        ).replace(tzinfo=timezone.utc)
        self.assertEqual(parse_date(date_str), date)


class UUIDTest(unittest.TestCase):
    def test_uuid(self):
        uuid = create_uuid()
        self.assertEqual(uuid.version, 4)


class AlgorythmsTest(unittest.TestCase):
    def test_not_exist_algo(self):
        with self.assertRaises(ValueError):
            get_algo("sha128")

    def test_sha1(self):
        orig, res = b"text random", "1a3e3bb2ff8cbbc17892f9d84da48882161f5f23"
        self.assertEqual(get_algo("sha1")(orig).hexdigest(), res)

    def test_md5(self):
        orig, res = b"text random", "00de4e08e2c76f54e696374a66502317"
        self.assertEqual(get_algo("md5")(orig).hexdigest(), res)

    def test_sha256(self):
        orig, res = (
            b"text random",
            "90e33e2185d50257415a37141536f215defdc87fa4a68869146def82faaeee6f",
        )
        self.assertEqual(get_algo("sha256")(orig).hexdigest(), res)

    def test_sha384(self):
        orig, res = (
            b"text random",
            "197ec7516df637032805b699adfcae51fb29a5d9271b2ca8fd21"
            "c13c5b370dd849b52522a02b9ccb731c4865ea6b1e4e",
        )
        self.assertEqual(get_algo("sha384")(orig).hexdigest(), res)

    def test_sha512(self):
        orig, res = (
            b"text random",
            "ed1df16e8d17c6c9ee461cad1e1b8565d9337b8d3555fa477ba8b4f3"
            "6d38d88491d701d429c80dac3193deafa0df17b736f67e58b297e41f2f48ca9959e54228",
        )
        self.assertEqual(get_algo("sha512")(orig).hexdigest(), res)


if __name__ == "__main__":
    unittest.main()
