#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- author: sgt911 -*-

from server import make_app
from tornado.ioloop import IOLoop
from config import config
from logging import getLogger

log = getLogger("server.run")

if __name__ == "__main__":
    production = config()["server"]["production_deploy"]
    # Creating server
    app = make_app(not production)

    # Enabling HTTP Server
    if production:
        from tornado.httpserver import HTTPServer

        server = HTTPServer(app)
        server.listen(config()["server"]["port"], "0.0.0.0")

        server.start(1)
    else:
        app.listen(config()["server"]["port"])
        log.warning("Dont run the debug mode on a production server")

    log.info("Server running on port %d", config()["server"]["port"])

    # IOLoop starting
    try:
        IOLoop.current().start()
    except KeyboardInterrupt:
        IOLoop.current().stop()
