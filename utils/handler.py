from logging import getLogger
from tornado.web import RequestHandler
from functools import wraps


class LogRequestHandler(RequestHandler):
    @wraps(RequestHandler.__init__)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.log = getLogger(f"server.routes.{self.__class__.__name__}")
