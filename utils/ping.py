import socket
from utils import Closable


class DNSException(Exception):
    pass


def ping_port(host: str, port: int) -> bool:
    """
    Ping a port checking if is open
    """

    with Closable(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        try:
            return sock.connect_ex((host, port)) == 0
        except socket.gaierror:
            raise DNSException(f"name {host} does not exists")
