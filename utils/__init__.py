from datetime import datetime
from uuid import UUID, uuid4
from tzlocal import get_localzone
from hashlib import sha1, sha256, sha384, sha512, md5

__hash_algos = {
    "md5": md5,
    "sha1": sha1,
    "sha256": sha256,
    "sha384": sha384,
    "sha512": sha512,
}


def tz_now() -> datetime:
    """Get datetime with the TZ of the machine"""
    return datetime.now().replace(tzinfo=get_localzone())


def create_uuid() -> UUID:
    """Create an UUIDv1 with the hostname of the machine"""
    return uuid4()


def get_algo(algo: str):
    """
    Get function of the algorythm.
    Examples:
        - md5
        - sha1
        - sha256
        - sha384
        - sha512
    """
    if algo not in __hash_algos:
        raise ValueError(f"algorythm {algo} not exists")

    return __hash_algos.get(algo)


def parse_date(date: str) -> datetime:
    try:
        return datetime.strptime(
            date.replace("Z", "+0000"), "%Y-%m-%dT%H:%M:%S%z"
        ).astimezone(get_localzone())
    except ValueError:
        return datetime.strptime(
            date.replace("Z", "+0000"), "%Y-%m-%dT%H:%M:%S.%f%z"
        ).astimezone(get_localzone())


class Closable:
    """A closable wrapper for instances with `.close()` method"""

    def __init__(self, instance):
        self._instance = instance

    def __enter__(self):
        return self._instance

    def __exit__(self, *args):
        self._instance.close()
