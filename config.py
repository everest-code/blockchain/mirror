from datetime import timedelta
from toml import loads as toml_decode
from functools import cache
from models import BlockchainConfig
from os import environ
from utils import get_algo


@cache
def config() -> dict:
    filename = environ.get("CONFIG", "./config.toml")
    with open(filename, "r") as file:
        data = file.read()

    return toml_decode(data)


def get_blockhain_conf() -> BlockchainConfig:
    cfg = config()["blockchain"]
    algo = get_algo(cfg["hash_algo"])

    if algo is None:
        raise KeyError(f"algorythm \"{cfg['hash_algo']}\" doesn't exists")

    return BlockchainConfig(
        difficulty=cfg["difficulty"],
        repeat_char=cfg["repeat_char"],
        tolerance=timedelta(minutes=cfg["tolerance"]),
        hash_algo=algo,
    )
