from __future__ import annotations
from abc import ABC, abstractmethod, abstractproperty, abstractstaticmethod
from dataclasses import asdict, dataclass, field
from json.decoder import JSONDecodeError
from typing import Callable, Dict, Optional, Union, List, Any
from datetime import datetime, timedelta, timezone
from utils import tz_now, create_uuid, parse_date, get_algo
from json import dumps as json_encode, loads as json_decode
from hashlib import sha512
from logging import getLogger
from uuid import UUID
from itertools import count

log = getLogger("models")


class _Hash(ABC, object):
    @abstractproperty
    def digest_size(self) -> int:
        raise NotImplementedError()

    @abstractproperty
    def block_size(self) -> int:
        raise NotImplementedError()

    @abstractproperty
    def name(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def __init__(self, data: bytes) -> None:
        raise NotImplementedError()

    @abstractmethod
    def copy(self) -> _Hash:
        raise NotImplementedError()

    @abstractmethod
    def digest(self) -> bytes:
        raise NotImplementedError()

    @abstractmethod
    def hexdigest(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def update(self, data: bytes) -> None:
        raise NotImplementedError()


_HashFunc = Callable[[bytes], _Hash]


class JSONSerializable(ABC):
    """Abstract class where serialize `self` into a JSON Serializable dict"""

    @abstractmethod
    def asjson(self) -> dict:
        raise NotImplementedError()

    @abstractstaticmethod
    def fromjson(data: dict) -> Any:
        raise NotImplementedError()

    def copy(self):
        return self.fromjson(self.asjson())


@dataclass
class Block(JSONSerializable):
    """
    A Block of the blockhain. The content is custom with a bytes.
    Recomendation: Serialize the content with JSON to get with property `content_dict`
    """

    content: Union[bytes, str] = field(repr=False)
    meta_id: UUID = field(default_factory=create_uuid)
    id: str = field(default=None)
    parent: Optional[str] = field(default=None)
    verify_token: Optional[int] = field(default=None)
    creation_date: datetime = field(default_factory=tz_now)
    verify_date: Optional[datetime] = field(default=None)

    @property
    def content_dict(self) -> Any:
        try:
            return json_decode(self.content)
        except JSONDecodeError:
            raise RuntimeError("the content is not an JSON")

    def asjson_str(self) -> str:
        data = self.asjson()
        del data["verify_date"]
        return json_encode(data, sort_keys=True, separators=(",", ":"))

    @staticmethod
    def fromjson(data: dict) -> Block:
        c_data = data.copy()
        if "meta_id" in c_data and c_data["meta_id"] is not None:
            c_data["meta_id"] = UUID(c_data["meta_id"])

        if "creation_date" in c_data:
            c_data["creation_date"] = parse_date(c_data["creation_date"])

        if "verify_date" in c_data and c_data["verify_date"] is not None:
            c_data["verify_date"] = parse_date(c_data["verify_date"])

        return Block(**c_data)

    def asjson(self) -> dict:
        """Return a JSON Serializable dict of a Block"""
        data = asdict(self)
        data["meta_id"] = str(self.meta_id)
        if isinstance(self.content, bytes):
            data["content"] = self.content.decode()
        data["creation_date"] = self.creation_date.astimezone(timezone.utc).isoformat()
        if self.verify_date is not None:
            data["verify_date"] = self.verify_date.astimezone(timezone.utc).isoformat()

        return data

    def ascontentjson(self) -> dict:
        """Return a JSON only with content, id and verify_date"""
        data = dict()
        data["id"] = str(self.meta_id)
        data["verify_date"] = self.verify_date.astimezone(timezone.utc).isoformat()
        try:
            data["content"] = self.content_dict
        except RuntimeError:
            data["content"] = self.content

        return data

    def get_hash(self, algorythm: _HashFunc) -> str:
        data = self.asjson_str().encode("utf-8")
        return algorythm(data).hexdigest()

    def verify(self, prefix: str, algorythm: _HashFunc) -> bool:
        """Check if the block is valid for add into the blockchain"""
        hash_code = self.get_hash(algorythm)
        if hash_code.startswith(prefix):
            self.id = hash_code
            self.verify_date = tz_now()
            return True
        return False


@dataclass
class BlockchainConfig(JSONSerializable):
    """Configuration of the blockchain, of difficulty and hash algorythm"""

    difficulty: int = field(default=3)
    repeat_char: str = field(default="0")
    tolerance: timedelta = field(default_factory=lambda: timedelta(minutes=5))
    hash_algo: _HashFunc = field(repr=False, default=sha512)

    @property
    def prefix(self) -> str:
        return self.repeat_char * self.difficulty

    @property
    def hash_algo_name(self) -> str:
        return self.hash_algo.__name__.replace("openssl_", "").lower()

    @staticmethod
    def fromjson(data: dict) -> BlockchainConfig:
        c_data = data.copy()

        if "tolerance" in c_data:
            c_data["tolerance"] = timedelta(seconds=c_data["tolerance"])

        if "hash_algo" in c_data:
            c_data["hash_algo"] = get_algo(c_data["hash_algo"])

        return BlockchainConfig(**c_data)

    def asjson(self) -> dict:
        data = asdict(self)
        data["hash_algo"] = self.hash_algo_name
        data["tolerance"] = self.tolerance.total_seconds()
        return data

    def change_difficulty(self, diff: timedelta):
        """Increase or decrease the diff"""
        if diff < (self.tolerance + timedelta(seconds=5)):
            self.difficulty += 1
            log.info(
                "Difficulty will be incremented, new difficulty %d",
                self.difficulty,
            )
        elif diff > (self.tolerance - timedelta(seconds=5)) and self.difficulty > 1:
            self.difficulty -= 1

    def __repr__(self) -> str:
        prefix, hash_algo, tolerance = (
            repr(self.prefix),
            self.hash_algo_name,
            repr(str(self.tolerance)),
        )

        params = f"prefix={prefix}, hash_algo={hash_algo}, tolerance={tolerance}"
        return f"BlockchainConfig({params})"


@dataclass
class Blockchain:
    """Blockchain with all basics methods to handle it"""

    config: BlockchainConfig = field(default_factory=BlockchainConfig)
    chain: List[Block] = field(default_factory=list)

    def find_by_meta_id(self, id: UUID) -> Optional[Block]:
        return {str(block.meta_id): block for block in self.chain}.get(str(id), None)

    def get_last(self) -> Optional[Block]:
        if len(self.chain) == 0:
            return None
        return self.chain[-1]

    def add_block(self, block: Block) -> int:
        if not block.verify(self.config.prefix, self.config.hash_algo):
            log.warning("Block %s was not accepted", block.meta_id)
            log.warning("JSON is: %s", block.asjson_str())
            log.warning("Hash got is: %s", block.get_hash(self.config.hash_algo))
            return -1

        last_block = self.get_last()
        if last_block is not None:
            previous_date = last_block.verify_date
            diff = block.verify_date - previous_date
            self.config.change_difficulty(diff)

        self.chain.append(block)

        return len(self.chain) - 1


@dataclass
class WaitBlock(JSONSerializable):
    """Waiting block to get from wait list"""

    content: Union[bytes, str] = field(repr=False)
    id: UUID = field(default_factory=create_uuid)
    creation_date: datetime = field(default_factory=tz_now)
    in_use: bool = field(init=False, default=False)

    @staticmethod
    def fromjson(data: str) -> WaitBlock():
        return WaitBlock(data)

    def asjson(self) -> dict:
        data = asdict(self)

        data["id"] = str(self.id)
        if isinstance(self.content, bytes):
            data["content"] = self.content.decode()
        data["creation_date"] = self.creation_date.isoformat()

        return data

    def as_block(self, parent: str) -> Block:
        data = asdict(self)

        del data["in_use"]
        data["verify_token"] = None
        data["parent"] = parent
        data["meta_id"] = data["id"]
        del data["id"]

        return Block(**data)


class WaitList(List[WaitBlock]):
    """WaitList/Queue for awaiting blocks"""

    def __init__(self):
        super().__init__()

    @property
    def as_hash_table(self) -> Dict[str, int]:
        return {str(block.id): idx for block, idx in zip(self, count())}

    def pop(self, id: UUID) -> WaitBlock:
        """Remove the id of a WaitBlock"""
        if (idx := self.as_hash_table.get(str(id), None)) is None:
            raise IndexError(id)
        else:
            return super().pop(idx)

    def toggle_in_use(self, id: UUID):
        """Enable/disable a WaitBlock because is already in use"""
        if (idx := self.as_hash_table.get(str(id), None)) is None:
            raise IndexError(id)
        else:
            self[idx].in_use = not self[idx].in_use

    @property
    def not_in_use(self) -> List[WaitBlock]:
        return [block for block in self if not block.in_use]

    def head(self, length: int = 1) -> List[WaitBlock]:
        """Get the X firsts WaitBlock of in WaitList"""
        return self.not_in_use[:length]

    def __repr__(self) -> str:
        inner_data = super().__repr__()[1:-1]
        return f"WaitList({inner_data})"
