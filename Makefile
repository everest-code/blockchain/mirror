PYTHON_BIN=python
PIP_BIN=${PYTHON_BIN} -m pip
UNITTEST_BIN=${PYTHON_BIN} -m unittest
CONF=./config.toml

.PHONY: start

start: .compile.sign run

# Dependencies
.install.sign:
	${PIP_BIN} install -r requirements.txt; \
	touch .install.sign

.compile.sign: .install.sign
# -x is to exclude with regex a path in ths case "./venv/" folder
	${PYTHON_BIN} -m compileall -x '^.*\/venv\/.*$$' .; \
	touch .compile.sign


# Principals
run: .install.sign
	${PYTHON_BIN} -m run

test: .compile.sign .install.sign
	${UNITTEST_BIN} tests/test_*.py -v

mine_benchmark:
	@echo "This is a test is not a production mining script"
	python __test__.py | tee -a res.txt

clean:
	rm -rdfv .*.sign